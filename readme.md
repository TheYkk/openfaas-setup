# Open faas cloud with k3s

### Create vm
dns settings
>faas.theykk.net to ip

### Create k3s cluster
https://gitlab.com/TheYkk/k3s-example

### Helm
https://www.digitalocean.com/community/tutorials/how-to-install-software-on-kubernetes-clusters-with-the-helm-package-manager

```bash
sudo snap install helm --classic

kubectl -n kube-system create serviceaccount tiller

kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller

helm init --service-account tiller
```
### Nginx setup
```bash
helm install stable/nginx-ingress --name ing 
```

### Stroge class
```bash
kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml
```



### Open faas
```bash
chmod +x install-openfaas.sh
./install-openfaas.sh
```
### Faas cli
```bash
chmod +x faas-cli.sh
./faas-cli.sh
```

### Load test
https://docs.openfaas.com/architecture/performance/
Create example http function 
```bash
hey -z=30s -q 5 -c 2 -m POST -d=Test https://faas.theykk.net/function/faasexe
```
