#!/bin/bash
echo "Applying namespaces"

kubectl apply -f https://raw.githubusercontent.com/openfaas/faas-netes/master/namespaces.yml

sha_cmd="sha256sum"
if [ ! -x "$(command -v $sha_cmd)" ]; then
    sha_cmd="shasum -a 256"
fi

if [ -x "$(command -v $sha_cmd)" ]; then
    sha_cmd="shasum"
fi

PASSWORD=$(head -c 16 /dev/urandom| $sha_cmd | cut -d " " -f 1)
echo -n $PASSWORD > password.txt

helm repo add openfaas https://openfaas.github.io/faas-netes/

kubectl -n openfaas create secret generic basic-auth \
--from-literal=basic-auth-user=admin \
--from-literal=basic-auth-password="$PASSWORD"

echo "Waiting for helm install to complete."

helm repo update \
 && helm upgrade openfaas --install openfaas/openfaas \
    --namespace openfaas  \
    --set basic_auth=true \
    --set functionNamespace=openfaas-fn